from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = [
    url(r'^user/', include('travelsky.apps.usermanager.user_urls')),
    url(r'^common/', include('travelsky.apps.common.urls')),
    url(r'^app/', include('travelsky.apps.application.urls')),
    url(r'^artifact/', include('travelsky.apps.artifact.urls')),
    url(r'^component/', include('travelsky.apps.component.urls')),
    # url(r'^jobtemplate/', include('travelsky.apps.jobtemplate.urls')),
    # url(r'^jobinstance/', include('travelsky.apps.jobinstance.urls')),
    url(r'^login/', include('travelsky.apps.usermanager.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^resource/', include('travelsky.apps.resource.urls')),
    url(r'^notify/', include('travelsky.apps.notify.urls')),
    url(r'^logmanage/', include('travelsky.apps.logmanage.urls')),

    ] + static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)