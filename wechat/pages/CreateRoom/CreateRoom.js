// CreateRoom.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    roles: [
      { name: 'player', value: '玩家', checked:'true' },
      { name: 'judge', value: '法官'},
    ],
    plarycount: { name: 'playercount', value: '玩家数4-16' },
    undercover: { name: 'undercover', value: '卧底数' },
    whiteboard: { name: 'whiteboard', value: '白板数' },
   
  },
  radioChange: function (e) {
    console.log('radio发生change事件，携带value值为：', e.detail.value)
  },
  getfocus_plaryer: function (e) {
    this.setData({
      plaryer_inputvalue:e.detail.value,
      undercover: { 
        name: 'undercover', 
        value: e.detail.value },
    })
  },

  blur_undercover: function (e) {
    this.setData({
      undercover: { 
        name: this.data.undercover.name, 
        value: this.data.plaryer_inputvalue}
    })
  },
  switch2Change:function(e){
    this.setData({
      whiteboard:{
        name: 'whiteboard',
        value: this.data.undercover.value
      } 

    })
    console.log('触发开关', this.data.plaryer_inputvalue)
  },
  bindindex: function () {
    console.log('触发取消');
    wx.navigateTo({
      url: '../index/index'
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})